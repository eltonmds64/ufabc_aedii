#include <stdio.h>

int main() {
    int number;

    printf("Enter an integer: ");
    scanf("%d", &number);

    // if else
    if (number % 2 == 0) {
        printf("%d is an even integer.\n", number);
    }
    else {
        printf("%d is an odd integer.\n", number);
    }

    // else if
    int number1, number2;
    printf("Enter two integers: ");
    scanf("%d %d", &number1, &number2);

    if(number1 == number2) {
        printf("Result: %d = %d\n",number1,number2);
    }
    else if (number1 > number2) {
        printf("Result: %d > %d\n", number1, number2);
    }
    else {
        printf("Result: %d < %d\n",number1, number2);
    }

    // for Loop
    int i;

    for (i = 0; i < 11; i++) {
        printf("%d ", i);
    }


    // while Loop

    i = 0;
    double numb, sum = 0;
    
    while (i <= 5) {
        printf("%d\n", i);
        ++i;
    }

    do {
        printf("Enter a number: ");
        scanf("%lf", &numb);
        sum += numb;
    }
    while (numb != 0.0);

    printf("Sum = %.2lf\n", sum);

    return 0;

    // switch case
    char operation;
    double n1, n2;

    printf("Enter an operator (+, -, *, /): ");
    scanf("%c", &operation);
    printf("Enter two operands: ");
    scanf("%lf %lf",&n1, &n2);

    switch(operation)
    {
        case '+':
            printf("%.1lf + %.1lf = %.1lf",n1, n2, n1+n2);
            break;

        case '-':
            printf("%.1lf - %.1lf = %.1lf",n1, n2, n1-n2);
            break;

        case '*':
            printf("%.1lf * %.1lf = %.1lf",n1, n2, n1*n2);
            break;

        case '/':
            printf("%.1lf / %.1lf = %.1lf",n1, n2, n1/n2);
            break;

        // operator doesn't match any case constant +, -, *, /
        default:
            printf("Error! operator is not correct");
    }


}