#include <stdio.h>

int main() {

    printf("Ola mundo!\n");

    // Fundamental Data Types
    int age = 23;
    float salary = 7445.07;
    double pi = 3.141512334;
    char test = 'h';
    short a;
    long b;
    long long c;
    long double d;

    printf("size of short = %d bytes\n", sizeof(a));
    printf("size of long = %d bytes\n", sizeof(b));
    printf("size of long long = %d bytes\n", sizeof(c));
    printf("size of long double= %d bytes\n", sizeof(d));

    return 0;
}