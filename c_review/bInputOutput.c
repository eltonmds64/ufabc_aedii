#include <stdio.h>

int main() {
    
    //  Output
    printf("C Programming");

    int testInteger = 5;
    printf("Number = %d\n", testInteger);

    float number1 = 13.5;
    double number2 = 12.4;

    printf("number1 = %f\n", number1);
    printf("number2 =  %lf\n", number2);
    
    char c = 'a';
    printf("character = %c\n", c);

    // Input
    int inputInteger;
    printf("Enter an integer: ");
    scanf("%d", &testInteger);
    printf("Number = %d\n", testInteger);

    float num1;
    double num2;

    printf("Enter a number: ");
    scanf("%f", &num1);
    printf("Enter another number: ");
    scanf("%lf", &num2);

    printf("num1 = %f\n", num1);
    printf("num2 = %lf\n", num2);

    // Characters
    char chr;
    printf("Enter a character: ");
    scanf(" %c", &chr);
    printf("You entered %c.\n", chr);

    printf("ASCII value is %d.\n", chr);
    
    // Multiple Values
    int a;
    float b;
    
    printf("Enter integer and then a float: ");
    scanf("%d%f", &a, &b);

    printf("You entered %d and %f\n", a, b);
    
    
    return 0;
}